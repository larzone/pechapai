<?php namespace Dekapai\Pechapai\Tests;

use Dekapai\Pechapai\Exceptions\RouteNotFoundException;
use \PHPUnit_Framework_TestCase;
use Dekapai\Pechapai\RouteCollection;
use Dekapai\Pechapai\Route;
use Dekapai\Pechapai\Router;

class RouterTest extends PHPUnit_Framework_TestCase
{

    /** @test */
    public function it_parses_a_basic_route_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::POST(
                'a/{b}/c/{d}/e',
                'MyController@MyMethod',
                'RequireAuth|XsrfProtection'
            )
        ]);

        $router = new Router($routeCollection, 'a/b/c/d/e', 'POST');

        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'POST');
        $this->assertEquals($route->getRouteMiddleware(), array(
            'RequireAuth',
            'XsrfProtection'
        ));
        $this->assertEquals($route->getController(), 'MyController');
        $this->assertEquals($route->getMethod(), 'MyMethod');
        $this->assertEquals($route->getParameters(), array(
            'b' => 'b',
            'd' => 'd'
        ));
    }

    /** @test */
    public function it_parses_a_route_with_middleware_given_as_array_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::POST(
                'a/{b}/c/{d}/e',
                'MyController@MyMethod',
                'SessionMiddleware|TimerMiddleware|DebugMiddleware'
            )
        ]);

        $router = new Router($routeCollection, 'a/b/c/d/e', 'POST');

        $route = $router->resolveRoute();
        $this->assertEquals($route->getRouteMiddleware(), array(
            'SessionMiddleware',
            'TimerMiddleware',
            'DebugMiddleware'
        ));

    }

    /** @test */
    public function it_parses_a_route_with_optional_parts_of_the_uri_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::GET(
                'showMembers(/onlyActive:{onlyActive})?(/sortBy:{sortBy})?',
                'MyController@MyMethod'
            ),

        ]);
        $router = new Router($routeCollection, 'showMembers', 'GET');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'GET');
        $this->assertEquals($route->getParameters(), []);

        $router = new Router($routeCollection, 'showMembers/onlyActive:true', 'GET');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'GET');
        $this->assertEquals($route->getParameters(), ['onlyActive' => 'true']);

        $router = new Router($routeCollection, 'showMembers/sortBy:joinDate', 'GET');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'GET');
        $this->assertEquals($route->getParameters(), ['sortBy' => 'joinDate']);

        $router = new Router($routeCollection, 'showMembers/onlyActive:false/sortBy:age', 'GET');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'GET');
        $this->assertEquals($route->getParameters(), ['onlyActive' => 'false', 'sortBy' => 'age']);

        $router = new Router($routeCollection, 'showMembers/something', 'GET');
        try {
            $router->resolveRoute();
            $this->assertTrue(false);
        } catch (RouteNotFoundException $e) {
            $this->assertTrue(true);
        }
    }

    /** @test */
    public function it_parses_a_route_where_two_parameters_are_exclusive_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::POST(
                'deleteMember(/deleteAll:{deleteAll}|/deleteById:{id})',
                'MyController@MyMethod'
            ),

        ]);
        $router = new Router($routeCollection, 'deleteMember/deleteAll:true', 'POST');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'POST');
        $this->assertEquals($route->getParameters(), ['deleteAll' => 'true']);

        $router = new Router($routeCollection, 'deleteMember/deleteById:56', 'POST');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'POST');
        $this->assertEquals($route->getParameters(), ['id' => '56']);

        $router = new Router($routeCollection, 'deleteMember/deleteAll:false/deleteById:56', 'POST');
        try {
            $router->resolveRoute();
            $this->assertTrue(false);
        } catch (RouteNotFoundException $e) {
            $this->assertTrue(true);
        }
    }

    /** @test */
    public function it_parses_a_route_which_requires_ints_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::GET(
                'showPosts/maxId/{maxId|int}/minId/{minId|int}',
                'MyController@MyMethod'
            ),

        ]);
        $router = new Router($routeCollection, 'showPosts/maxId/1745/minId/842', 'GET');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'GET');
        $this->assertEquals($route->getParameters(), ['maxId' => 1745, 'minId' => 842]);

        $router = new Router($routeCollection, 'showPosts/maxId/whoops/minId/842', 'GET');
        try{
            $router->resolveRoute();
            $this->assertTrue(false);
        } catch (RouteNotFoundException $e) {
            $this->assertTrue(true);
        }
    }

    /** @test */
    public function it_parses_a_route_which_requires_dates_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::GET(
                'showPosts/from:{from|date}/to:{to|date}',
                'MyController@MyMethod'
            ),

        ]);
        $router = new Router($routeCollection, 'showPosts/from:2014-12-24/to:2015-12-24', 'GET');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'GET');
        $this->assertEquals($route->getParameters(), ['from' => '2014-12-24', 'to' => '2015-12-24']);

        $router = new Router($routeCollection, 'showPosts/from:2014-12-24/to:2015-02-31', 'GET');
        try {
            $router->resolveRoute();
            $this->assertTrue(false);
        } catch (RouteNotFoundException $e) {
            $this->assertTrue(true);
        }
    }

    /** @test */
    public function it_parses_a_route_which_requires_booleans_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::GET(
                'showPosts/showAll:{showAll|bool}(/deleted:{deleted|boolean})?',
                'MyController@MyMethod'
            ),

        ]);
        $router = new Router($routeCollection, 'showPosts/showAll:true/deleted:false/', 'GET');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'GET');
        $this->assertEquals($route->getParameters(), ['showAll' => true, 'deleted' => false]);

        $router = new Router($routeCollection, 'showPosts/showAll:shit/', 'GET');
        try {
            $router->resolveRoute();
            $this->assertTrue(false);
        } catch (RouteNotFoundException $e) {
            $this->assertTrue(true);
        }
    }

    /** @test */
    public function it_parses_a_regex_in_the_placeholder_and_assigns_it_a_name_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::GET(
                'showPosts/byUser/{uid_[a-z]+|name(userid)}',
                'MyController@MyMethod'
            ),

        ]);
        $router = new Router($routeCollection, 'showPosts/byUser/uid_someuser', 'GET');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'GET');
        $this->assertEquals($route->getParameters(), ['userid' => 'uid_someuser']);
    }

    /** @test */
    public function it_puts_the_lotion_in_the_basket_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::GET(
                'posts(/uid:{uid|int}|/from:{from|date}|/to:{to|date})*',
                'MyController@MyMethod'
            ),

        ]);
        $router = new Router($routeCollection, 'posts/from:2010-01-01/uid:23', 'GET'); // dude
        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'GET');
        $this->assertEquals($route->getParameters(), ['from' => '2010-01-01', 'uid' => 23]);
    }

    /** @test */
    public function it_handles_a_complex_route_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::GET(
                'api/dog/getdogs(/pageno:{pageno|int}|/user_id:{user_id|int}|/dogs_per_page:{dogs_per_page|int}|/filter:{filter}|/options:{options}|/kennel:{kennel})*',
                'PublicAPI\dogDataController@getdogs'
            ),
        ]);
        $router = new Router($routeCollection, 'api/dog/getdogs/pageno:1/dogs_per_page:24/filter:type=any/options:dead=no,sortby=date', 'GET');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getParameters(), [
            'pageno' => 1,
            'dogs_per_page' => 24,
            'filter' => 'type=any',
            'options' => 'dead=no,sortby=date'
        ]);
    }
    
    /** @test */
    public function it_handles_put_and_delete_requests_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::PUT(
                'api/dog',
                'PublicAPI\dogDataController@newDog'
            ),
            Route::DELETE(
                'api/dog',
                'PublicAPI\dogDataController@deleteDog'
            ),
        ]);

        $router = new Router($routeCollection, 'api/dog', 'PUT');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getMethod(), 'newDog');

        $router = new Router($routeCollection, 'api/dog', 'DELETE');
        $route = $router->resolveRoute();
        $this->assertEquals($route->getMethod(), 'deleteDog');
    }

    /** @test */
    public function it_does_stuff_with_constructor_arguments_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::GET(
                'getpost/{Domain\Post@id|int}',
                'MyController@MyMethod'
            )
        ]);

        $router = new Router($routeCollection, 'getpost/174', 'GET');

        $route = $router->resolveRoute();
        $this->assertEquals($route->getRequestMethod(), 'GET');
        $this->assertEquals($route->getParameters(), array(
            'Domain\Post@id' => 174
        ));
    }

    /** @test */
    public function it_handles_closures_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::GET(
                'about', function() { return "about page"; }
            )
        ]);

        $router = new Router($routeCollection, 'about', 'GET');
        $route = $router->resolveRoute();
        if ($route->isClosure()) {
            $handle = $route->getHandle();
            $this->assertEquals('about page', $handle());
        } else {
            $this->assertFalse(true);
        }
    }

    /** @test */
    public function it_can_pass_values_to_closures_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::GET(
                'hello/{name}', function($name) { return "hello {$name}"; }
            )
        ]);

        $router = new Router($routeCollection, 'hello/arnold', 'GET');
        $route = $router->resolveRoute();
        if ($route->isClosure()) {
            $handle = $route->getHandle();
            $this->assertEquals('hello arnold', $handle($route->getParameters()['name']));
        } else {
            $this->assertFalse(true);
        }
    }

    /** @test */
    public function it_can_do_custom_handlers_or_else_it_gets_the_hose_again()
    {
        $routeCollection = new RouteCollection([
            Route::POST(
                'register/{email|email}', '\MyController@myMethod'
            )
        ]);
        $router = new Router($routeCollection, 'register/bill.gates@microsoft.com', 'POST');
        $router->customHandler('email', function($email) {
            return (bool) filter_var($email, FILTER_VALIDATE_EMAIL);
        });
        $route = $router->resolveRoute();
        $this->assertEquals('bill.gates@microsoft.com', $route->getParameters()['email']);

        $router = new Router($routeCollection, 'register/~bill".gates@mic½rosoft.com', 'POST');
        $router->customHandler('email', function($email) {
            return (bool) filter_var($email, FILTER_VALIDATE_EMAIL);
        });
        try {
            $route = $router->resolveRoute();
            $this->assertFalse(true);
        } catch (RouteNotFoundException $e) {
            $this->assertTrue(true);
        }

    }

}

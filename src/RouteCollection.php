<?php namespace Dekapai\Pechapai;

class RouteCollection implements \Iterator
{

    private $position;
    private $registeredRoutes = array();

    public function __construct(array $routes)
    {
        foreach ($routes as $route)
        {
            if ($route instanceof Route) {
                $this->registeredRoutes[] = $route;
            } else {
                throw new \InvalidArgumentException("All elements must be of type Route.");
            }
        }
        $this->position = 0;
    }

    // Iterator functions

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->registeredRoutes[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->registeredRoutes[$this->position]);
    }
}

<?php namespace Dekapai\Pechapai;

class Route
{
    private $requestMethod;
    private $uri;
    private $controller;
    private $method;
    private $middleware;
    private $parameters;
    private $isClosure;
    private $handle;

    private function __construct($requestMethod, $uri, $controllerAndMethod, $middleware)
    {
        if (is_string($controllerAndMethod)) {
            $controllerAndMethodArr = explode('@', $controllerAndMethod);
            if (count($controllerAndMethodArr) != 2) throw new \InvalidArgumentException("Controller and method must be separated by @");
            $this->controller = $controllerAndMethodArr[0];
            $this->method = $controllerAndMethodArr[1];
            $this->isClosure = false;
        } else {
            $this->isClosure = true;
            $this->handle = $controllerAndMethod;
        }
        $this->requestMethod = $requestMethod;
        $this->uri = trim($uri, '/');
        $this->middleware = $middleware;
    }

    public static function GET($uri, $controllerAndMethod, $preControllers = null)
    {
        return new static('GET', $uri, $controllerAndMethod, $preControllers);
    }

    public static function POST($uri, $controllerAndMethod, $preControllers = null)
    {
        return new static('POST', $uri, $controllerAndMethod, $preControllers);
    }

    public static function PUT($uri, $controllerAndMethod, $preControllers = null)
    {
        return new static('PUT', $uri, $controllerAndMethod, $preControllers);
    }

    public static function DELETE($uri, $controllerAndMethod, $preControllers = null)
    {
        return new static('DELETE', $uri, $controllerAndMethod, $preControllers);
    }

    public function getRequestMethod() { return $this->requestMethod; }
    public function getUri() { return $this->uri; }
    public function getController() { return $this->controller; }
    public function getMethod() { return $this->method; }
    public function getParameters() { return $this->parameters; }
    public function getRouteMiddleware() {
        if ($this->middleware == null) return array();
        if (is_string($this->middleware)) {
            return explode('|', $this->middleware);
        }
        return array();
    }
    public function setParameters($parameters) { $this->parameters = $parameters; }
    public function isClosure() { return $this->isClosure; }
    public function getHandle() {
        if (!$this->isClosure()) throw new \UnexpectedValueException("The route is not a function handle.");
        return $this->handle;
    }
}

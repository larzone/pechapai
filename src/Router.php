<?php namespace Dekapai\Pechapai;

use Dekapai\Pechapai\Exceptions\RouteNotFoundException;

class Router
{
    private $routeCollection;
    private $uri;
    private $requestMethod;
    private $customHandlers;

    public function __construct(RouteCollection $routeCollection, $uri, $requestMethod)
    {
        $this->routeCollection = $routeCollection;
        $this->uri = trim($uri, '/');
        $this->requestMethod = $requestMethod;
        $this->customHandlers = array();
    }

    public function resolveRoute()
    {
        foreach ($this->routeCollection as $route) {
            $result = $this->processRoute($route);
            if (false === $result) continue;
            return $result;
        }
        throw new RouteNotFoundException;
    }

    private function processRoute(Route $route)
    {
        $method = $route->getRequestMethod(); if ($this->requestMethod !== $method) return false;

        $regex = $this->replacePlaceholders($route);

        if (!preg_match($regex, $this->uri, $match)) {
            return false;
        }

        $namedParameters = $this->getNamedParameters($route);
        array_shift($match);
        $params = array();

        for ($i = 0; $i < count($match); $i++) {
            $key = $namedParameters[$i];
            $paramValue = $match[$i];
            if (!strlen($paramValue)) continue;

            if (strstr($key, '|')) {

                list($key, $modifier) = explode('|', $key);
                $parameterToModifier = null;
                if (strstr($modifier, '(')) {
                    preg_match('/(.*?)\((.*?)\)/', $modifier, $m);
                    $modifier = $m[1];
                    $parameterToModifier = $m[2];
                }
                switch ($modifier) {
                    case('int'):
                        if (!preg_match('/^\d+$/', $paramValue)) return false;
                        $paramValue = (int) $paramValue;
                        break;
                    case('date'):
                        if (!preg_match('/\d{4}-\d{2}-\d{2}/', $paramValue)) return false;
                        $date = new \DateTime($paramValue);
                        if ($date->format('Y-m-d') != $paramValue) return false;
                        break;
                    case('bool'):
                    case('boolean'):
                        $paramValue = strtolower($paramValue);
                        if (!in_array($paramValue, ['true', 'false'])) return false;
                        $paramValue = ($paramValue == 'true');
                        break;
                    case('name'):
                        $key = $parameterToModifier;
                        break;
                    default:
                        if (isset($this->customHandlers[$modifier])) {
                            $handle = $this->customHandlers[$modifier];
                            $matched = $handle($paramValue);
                            if (!$matched) return false;
                        }
                        break;
                }
            }

            $params[$key] = $paramValue;
        }

        $route->setParameters($params);
        return $route;
    }

    private function replacePlaceholders(Route $route)
    {
        $temp = str_replace('/', '\\/', $route->getUri());
        $temp = str_replace('(', '(?i:', $temp);
        $regex = '/^' . preg_replace('/\{[^\/]+\}/', '([^\/]+?)', $temp) . '$/';
        return $regex;
    }

    private function getNamedParameters(Route $route)
    {
        preg_match_all('/\{([^}]+)\}/', $route->getUri(), $matches);
        $namedParameters = $matches[1];
        return $namedParameters;
    }

    public function customHandler($handler, $handle)
    {
        $this->customHandlers[$handler] = $handle;
    }

}

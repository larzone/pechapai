# Pechapai

Basic router that does some weird shit.

## Example using a class method

    $routeCollection = new RouteCollection([
        Route::POST(
            'a/{b}/c/{d}/e',                // Route variables are caught within {} blocks.
            '\MyController@MyMethod',       // Class (full namespace) and method name, separated by @
            'RequireAuth|XsrfProtection'    // Optional: Route middleware separated by |
        ), // ... more Routes
    ]);

    $router = new Router($routeCollection, 'a/b/c/d/e', 'POST'); // Pass collection, uri and method

    $route = $router->resolveRoute();       // Returns a Route object
    print   $route->getRequestMethod();     // Prints 'POST'
    print_r($route->getRouteMiddleware());  // Prints array('RequireAuth', 'XsrfProtection')
    print   $route->getController();        // Prints '\MyController'
    print   $route->getMethod()             // Prints 'MyMethod'
    print_r($route->getParameters())        // Prints array('b' => 'b', 'd' => 'd')

## Regex matching of routes

This matches all uri's of the form posts/ followed by any number of letters

    Route::GET(
        'posts/[a-z]+',
        'MyController@MyMethod'
    )

## Capturing the result of a matched regex

If we write the regex within {} and append the special qualifier |name() we can assign a name to the match.

    Route::GET(
        'posts/{[a-z]+|name(post_id)}',
        'MyController@MyMethod'
    )
    // ...
    print $route->getParameters()['post_id'];

## Optional arguments

Using standard regexes we can make a route argument optional.

    Route::GET(
        'posts(/{showall})?',
        'MyController@MyMethod'
    )
    // ...
    print_r(isset($route->getParameters()['showall']));

On successful match, the parameter 'showall' will have value 'showall'.


## Capturing ints, dates and booleans

Add qualifiers after a | character within the catch group.

    Route::GET(
        'get/all:{all|bool}(/deleted:{deleted|boolean})?(/date:{date|date})',
        'MyController@MyMethod'
    )

bool and boolean are synonymous and allow values `true` and `false` (case insensitive). Date matches YYYY-MM-DD.

## Bind a custom match handler before resolving the route.

You can define a boolean function that returns true or false depending on the argument matches the expected format.

    $routeCollection = new RouteCollection([
        Route::POST(
            'register/{email|email}', '\MyController@myMethod'
        )
    ]);
    $router = new Router($routeCollection, 'register/bill.gates@microsoft.com', 'POST');
    $router->customHandler('email', function($email) {
        return (bool) filter_var($email, FILTER_VALIDATE_EMAIL);
    });
    // Now email is available as a descriptor in route match groups.
    $route = $router->resolveRoute();

## Optional arguments in any order

Using regexes, you can define routes where these are equivalent: `posts/id/4/user/5`, `posts/user/5/id/4`

    Route::POST(
        'posts(/id/{id|int}|/user/{user|int})*', '\MyController@myMethod'
    )

## Example of using closures

You can pass in a closure instead of a class method string.

    $routeCollection = new RouteCollection([
        Route::GET(
            'about', function() { return "about page"; }
        ),
        Route::GET(
            'contact', function() { return "contact page"; }
        )
    ]);

The function is not actually invoked when the route is resolved, but it can be returned as a handle after checking
that the route is mapped to a closure. It is supposed to work in conjunction with a DI container and not be
called directly. Note that you can also have user-defined types in the declaration of the closure.

    $router = new Router($routeCollection, 'about', 'GET');
    $route = $router->resolveRoute();
    if ($route->isClosure()) {
        $handle = $route->getHandle();
        $result = $handle();  // Invoking here
        // Real use case:
        //$myCustomDiContainer->invoke($handle);  // Injects dependencies (DI container not provided here)
    }
